def get_left_half(bits):
    return bits[:len(bits)//2]


def get_right_half(bits):
    return bits[len(bits)//2:]


def permutate(original, fixed_key):
    return ''.join(original[i - 1] for i in fixed_key)
