from constants import EP, S0, S1, P4
from utils import get_left_half, get_right_half, permutate


def xor(bits, key):
    return ''.join(str(((bit + key_bit) % 2)) for bit, key_bit in
                   zip(map(int, bits), map(int, key)))


def SBox(bits, sbox):
    row = int(bits[0] + bits[3], 2)
    col = int(bits[1] + bits[2], 2)
    return '{0:02b}'.format(sbox[row][col])


def round(bits, key):
    L = get_left_half(bits)
    R = get_right_half(bits)  # The input sequence is slicing into 2 parts

    bits = permutate(R, EP)  # Extend the right side of R to 8 bits using the extension rule E / p

    bits = xor(bits, key)  # XOR operation

    left_SBox_cipher = SBox(get_left_half(bits), S0)
    right_SBox_cipher = SBox(get_right_half(bits), S1)  # process the result with S-blocks (S-box).

    bits = permutate(left_SBox_cipher + right_SBox_cipher, P4)
    return xor(bits, L)
