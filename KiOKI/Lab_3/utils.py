def convert_letters_to_numbers(letters):
    numbers = []
    letters = letters.lower()

    for letter in letters:
        number = ord(letter) - 96
        numbers.append(number)

    return numbers


def convert_numbers_to_letters(numbers):
    letters = []

    for number in numbers:
        number = chr(number + 96)
        letters.append(number)

    return letters
