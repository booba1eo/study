from key_generator import generate_keys
from constants import IP, IP_INVERSE
from utils import permutate, get_right_half
from round import round


def encrypt(key, source_text):
    first_key, second_key = generate_keys(key)
    bits = permutate(source_text, IP)

    temp = round(bits, first_key)
    bits = get_right_half(bits) + temp
    bits = round(bits, second_key)

    encrypted_text = permutate(bits + temp, IP_INVERSE)
    print('Encrypted text: ' + encrypted_text)
    return encrypted_text


def decrypt(key, encrypted_text):
    first_key, second_key = generate_keys(key)
    bits = permutate(encrypted_text, IP)

    temp = round(bits, second_key)
    bits = get_right_half(bits) + temp
    bits = round(bits, first_key)

    source_text = permutate(bits + temp, IP_INVERSE)
    print('Source text: ' + source_text)
    return source_text


if __name__ == "__main__":
    from sys import argv
    try:
        operation = argv[1]
        key = argv[2]
        text = argv[3]
        is_decrypt = bool(operation == "-d")
        is_encrypt = bool(operation == "-e")

        if is_decrypt:
            decrypt(key, text)
        elif is_encrypt:
            encrypt(key, text)
        else:
            raise IndexError
    except IndexError:
        print('Usage: python3 -m cripta.encryptor <-e|-d> <KEY> <TEXT>')
