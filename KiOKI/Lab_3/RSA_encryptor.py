from utils import convert_letters_to_numbers, convert_numbers_to_letters
from RSA_keygen import key_pair_generator


def encrypt(open_key, source_text):
    converted_text = convert_letters_to_numbers(source_text)
    encrypted_text = []
    e, r = open_key

    for letter in converted_text:
        encrypted_letter = pow(letter, e) % r
        encrypted_text.append(encrypted_letter)

    return encrypted_text


def decrypt(private_key, encrypted_text):
    decrypted_text = []
    d, r = private_key

    for number in encrypted_text:
        decrypted_number = pow(number, d) % r
        decrypted_text.append(decrypted_number)

    decrypted_text = convert_numbers_to_letters(decrypted_text)
    return decrypted_text


if __name__ == "__main__":
    p = int(input('Enter the value of p = '))
    q = int(input('Enter the value of q = '))
    open_key, private_key = key_pair_generator(p, q)

    source_text = str(input('Enter the value of text = '))
    encrypted_text = encrypt(open_key, source_text)
    decrypted_text = decrypt(private_key, encrypted_text)
    print('encrypted_text', encrypted_text)
    print('decrypted_text', decrypted_text)
