#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from utils import convert_letters_to_numbers


def hash(n, H0, source_text):
    Hi = H0
    converted_text = convert_letters_to_numbers(source_text)
    h_M = None

    text_len = len(converted_text)-1
    for index, letter in enumerate(converted_text):
        Hi = pow((Hi + letter), 2) % n
        if index == text_len:
            h_M = Hi
    return h_M


if __name__ == "__main__":
    n = int(input('Enter the value of n = '))
    H0 = int(input('Enter the value of H0 = '))
    source_text = str(input('Enter the value of text = '))

    hash_value = hash(n, H0, source_text)
    print('hash value: ', hash_value)
