from utils import permutate, get_left_half, get_right_half
from constants import P8, P10


def generate_first_key(left_key, right_key):
    left_key_rot = left_key[1:] + left_key[:1]
    right_key_rot = right_key[1:] + right_key[:1]
    key_rot = left_key_rot + right_key_rot
    return permutate(key_rot, P8)


def generate_second_key(left_key, right_key):
    left_key_rot = left_key[3:] + left_key[:3]
    right_key_rot = right_key[3:] + right_key[:3]
    key_rot = left_key_rot + right_key_rot
    return permutate(key_rot, P8)


def generate_keys(key):
    p10key = permutate(key, P10)
    left = get_left_half(p10key)
    right = get_right_half(p10key)

    first_key = generate_first_key(left, right)
    second_key = generate_second_key(left, right)
    return first_key, second_key
