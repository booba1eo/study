from math import gcd


def xgcd(a, b):
    x0, x1, y0, y1 = 0, 1, 1, 0
    while a != 0:
        (q, a), b = divmod(b, a), a
        y0, y1 = y1, y0 - q * y1
        x0, x1 = x1, x0 - q * x1
    return y0


def get_open_exp(t):
    for e in range(2, t):
        if gcd(e, t) == 1:
            break
    return e


def key_pair_generator(p, q):
    n = p*q
    f_n = (p-1)*(q-1)

    e = get_open_exp(f_n)
    d = xgcd(f_n, e)
    if not d >= 0:
        d += f_n

    open_key = (e, n)
    private_key = (d, n)
    print(f'Open key: {open_key}')
    print(f'Private key: {private_key}')
    return open_key, private_key


if __name__ == "__main__":
    p = int(input('Enter the value of p = '))
    q = int(input('Enter the value of q = '))

    key_pair_generator(p, q)
