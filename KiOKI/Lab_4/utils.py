#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from constants import ALPHABET


def convert_letters_to_numbers(letters):
    numbers = []
    letters = letters.upper()

    for letter in letters:
        if letter in ALPHABET:
            number = ALPHABET[letter]
            numbers.append(number)

    return numbers
